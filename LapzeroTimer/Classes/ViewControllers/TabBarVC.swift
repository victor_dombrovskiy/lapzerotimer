//
//  TabBarVC.swift
//  LapzeroTimer
//
//  Created by Victor on 9/23/16.
//  Copyright © 2016 Bananaapps. All rights reserved.
//

import UIKit

class TabBarVC: UITabBarController {
    
    public var isLocked: Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.delegate = self
    }
    
}

extension TabBarVC: UITabBarControllerDelegate {
    
    func tabBarController(_ tabBarController: UITabBarController, shouldSelect viewController: UIViewController) -> Bool {
        
        if viewController.isEqual(self.selectedViewController) {
            // preventing double-tap (pops VC from navigations stack)
            return false
        } else if isLocked {
            // checking if switching is locked
            return false
        } else {
            return true
        }
    }
}
