//
//  AthleteCollectionViewCell.swift
//  LapzeroTimer
//
//  Created by Victor on 9/23/16.
//  Copyright © 2016 Bananaapps. All rights reserved.
//

import UIKit

class AthleteCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet var indicators: [UIView]!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        configure()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        configure()
    }
    
    func configure() {
        self.contentView.backgroundColor = UIColor.appIndicatorBoxColor()
        self.contentView.layer.cornerRadius = 4
        self.contentView.clipsToBounds = true
        self.contentView.layer.borderColor = UIColor.appGrayColor().cgColor
        self.contentView.layer.borderWidth = 1
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        for anIndicator in self.indicators {
            // round corners once
            if anIndicator.clipsToBounds == false {
                anIndicator.layer.cornerRadius = 4
                anIndicator.clipsToBounds = true
                anIndicator.layer.borderColor = UIColor.black.cgColor
                anIndicator.layer.borderWidth = 1
            }
        }
    }
}
