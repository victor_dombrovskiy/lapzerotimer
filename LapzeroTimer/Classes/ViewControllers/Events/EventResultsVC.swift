//
//  EventResultsVC.swift
//  LapzeroTimer
//
//  Created by Victor on 9/23/16.
//  Copyright © 2016 Bananaapps. All rights reserved.
//

import Foundation
import UIKit

/*
 Output format:
 two lines:
 first line - intervals between checkins
 second line - intervals between 0 and checkin
 
 Example: times are 00:10 00:15 00:17
 1st line: 00:10 00:05 00:02
 2nd line: 00:10 00:15 00:17
 */

class EventResultsVC: UIViewController {
    
    // MARK: - Properties
    
    @IBOutlet weak var textView: UITextView!
    var event: Event!
    
    // MARK: - Initialization
    
    func configureOnload() {
        
        let rightBarButton = UIBarButtonItem(image: #imageLiteral(resourceName: "Share-bar"), style: .plain, target: self, action: #selector(rightBarButtonTapped(sender:)))
        navigationItem.rightBarButtonItems = [rightBarButton]
        
        let leftBarButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(leftBarButtonTapped(sender:)))
        navigationItem.leftBarButtonItem = leftBarButton
        
        self.navigationItem.title = event.name
    }
    
    // MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureOnload()
        textView.attributedText = event.resultsStringFormatted()
    }
    
    // UITextView text content doesn't start from the top, solution:
    override func viewWillAppear(_ animated: Bool) {
        textView.isScrollEnabled = false
    }
    // and this one too:
    override func viewDidAppear(_ animated: Bool) {
        textView.isScrollEnabled = true
    }
    
    // MARK: - Actions
    
    func rightBarButtonTapped(sender:UIButton) {
        self.presentActivityShareMenu(activityItems: [event.resultsStringFormatted()], sourceView: sender)
    }
    
    /// Cancel button
    func leftBarButtonTapped(sender:UIButton) {
        // pop to root VC
        let _ = self.navigationController?.popToRootViewController(animated: true)
    }
    
}



