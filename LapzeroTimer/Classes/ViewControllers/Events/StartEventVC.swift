//
//  StartEventVC.swift
//  LapzeroTimer
//
//  Created by Victor on 9/21/16.
//  Copyright © 2016 Bananaapps. All rights reserved.
//

import UIKit
import RealmSwift

class StartEventVC: UIViewController {
    
    // MARK: - Properties
    
    @IBOutlet weak var button: UIButton! {
        didSet {
            button.backgroundColor = UIColor.appGreenColor()
            button.layer.cornerRadius = 4
            button.clipsToBounds = true
        }
    }
    @IBOutlet weak var timerLabel: UILabel! {
        didSet {
            timerLabel.font = timerLabel.font.monospacedDigitFont
        }
    }
    @IBOutlet weak var tableView: UITableView!
    
    // data
    let realm = try! Realm()
    var event: Event!
    
    // MARK: - Initialization
    
    func configureOnload() {
        tableView.dataSource = self
        tableView.delegate = self
        tableView.isEditing = true
        self.navigationItem.title = event?.name
    }
    
    // MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureOnload()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        tableView.reloadData()
    }
    
    // MARK: - Actions
    
    // START button
    @IBAction func buttonPressed(_ sender: UIButton) {
        self.playClickSound()
        
        // clear previous data, update NSDate instance
        realm.beginWrite()
        event.resetAllResults()
        event.date = Date()
        try! realm.commitWrite()
        
        // push next vc
        let timerVC = Constants.Storyboards.MainStoryboard.instantiateViewController(withIdentifier: "EventTimerVC") as! EventTimerVC
        timerVC.event = event
        self.navigationController?.pushViewController(timerVC, animated: false)
        
        // remove self from stack
        var viewControllers = self.navigationController!.viewControllers
        viewControllers.remove(at: viewControllers.count-2)
        self.navigationController?.viewControllers = viewControllers
    }
    
}

extension StartEventVC: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return event.athletes.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "AthlettesListCell", for: indexPath) as! AthlettesListCell
        cell.nameLabel.text = event.athletes[indexPath.row].name
        cell.showsReorderControl = true
        return cell
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    // rearrange items
    
    func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, moveRowAt sourceIndexPath: IndexPath, to destinationIndexPath: IndexPath) {
        // save data
        realm.beginWrite()
        let item = event.athletes[sourceIndexPath.row]
        event.athletes.remove(at: sourceIndexPath.row)
        event.athletes.insert(item, at: destinationIndexPath.row)
        try! realm.commitWrite()
    }
}

extension StartEventVC: UITableViewDelegate {
    
    // hides delete button
    func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCellEditingStyle {
        return .none
    }
    
    // removes left spacing (hidden del button) 
    func tableView(_ tableView: UITableView, shouldIndentWhileEditingRowAt indexPath: IndexPath) -> Bool {
        return false
    }
    
}
