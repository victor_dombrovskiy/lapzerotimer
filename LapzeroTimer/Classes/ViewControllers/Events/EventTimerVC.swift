//
//  EventTimerVC.swift
//  LapzeroTimer
//
//  Created by Victor on 9/20/16.
//  Copyright © 2016 Bananaapps. All rights reserved.
//

import Foundation
import UIKit
import RealmSwift

class EventTimerVC: UIViewController {
    
    // MARK: - Properties
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var buttonStop: UIButton! {
        didSet {
            buttonStop.backgroundColor = UIColor.appRedColor()
            buttonStop.layer.cornerRadius = 4
            buttonStop.clipsToBounds = true
        }
    }
    
    var redrawUItimer: Timer!               // redraw timer
    
    // main timer:
    @IBOutlet weak var mainTimercontainerView: UIView!    
    @IBOutlet weak var timerLabel: UILabel! {
        didSet {
            timerLabel.font = timerLabel.font.monospacedDigitFont
        }
    }
    var startTime: TimeInterval!
    var currentTime: TimeInterval! {
        get {
            return Date.timeIntervalSinceReferenceDate - self.startTime
        }
    }
    
    // relay timer:
    @IBOutlet weak var relayContainerView: UIView!
    @IBOutlet weak var relayAthleteLabel: UILabel!
    @IBOutlet weak var relayTimerLabel: UILabel! {
        didSet {
            relayTimerLabel.font = relayTimerLabel.font.monospacedDigitFont
        }
    }
    var relayStartTime: TimeInterval!
    var relayCurrentTime: TimeInterval! {
        get {
            return Date.timeIntervalSinceReferenceDate - self.relayStartTime
        }
    }
    var relaySelectedAthleteIndex: Int = 0 // first by deault
    
    // data
    var event: Event!
    let realm = try! Realm()
    
    // MARK: - Initialization
    
    func configureOnload() {
        self.navigationItem.title = event.name
        
        let leftBarButton = UIBarButtonItem(barButtonSystemItem: .cancel, target: self, action: #selector(leftBarButtonTapped(sender:)))
        navigationItem.leftBarButtonItem = leftBarButton
        
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.backgroundColor = UIColor.appLightGrayColor()
        
        // timers
        startTime = Date.timeIntervalSinceReferenceDate
        redrawUItimer = Timer.scheduledTimer(timeInterval: 0.005, target: self, selector: #selector(updateTimerLabels), userInfo: nil, repeats: true)
        RunLoop.current.add(redrawUItimer, forMode: .commonModes) // custom UI elements are not being updated while UIScrollView is scrolled
        timerLabel.text = ""
        
        // check if relay:
        if isRelayMode() {
            // setup relayTimer
            relayStartTime = Date.timeIntervalSinceReferenceDate
            relayTimerLabel.text = ""
            relayAthleteLabel.text = event.athletes.first?.name ?? ""
            relaySelectedAthleteIndex = 0
        } else {
            // relay disabled
            // hide view,
            relayContainerView.removeFromSuperview()
            // add constraint
            collectionView.topAnchor.constraint(equalTo: mainTimercontainerView.bottomAnchor, constant: 0).isActive = true
        }
        
        self.updateTimerLabels()
    }
    
    // MARK: - Lifecycle
    
    func isRelayMode() -> Bool {
        return event.isRelay
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureOnload()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        // disable sleep once VC started
        Constants.AppDelegate.preventSleepMode(prevent: true)
        // lock tabbar
        Constants.AppDelegate.tabBarController.isLocked = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        // enable sleep mode
        Constants.AppDelegate.preventSleepMode(prevent: false)
        // unlock tabbar
        Constants.AppDelegate.tabBarController.isLocked = false
        super.viewWillDisappear(animated)
    }
    
    func updateTimerLabels() {
        timerLabel.text = currentTime.stringFormatted()
        if isRelayMode() {
            relayTimerLabel.text = relayCurrentTime.stringFormatted()
        }
    }
    
    // MARK: - Actions
    
    @IBAction func buttonStopPressed(_ sender: UIButton) {
        // stop timer and save results, go next vc
        print("stop at - \(currentTime.stringFormatted())")
        redrawUItimer.invalidate()
        sender.isEnabled = false
        self.playClickSound()
        
        // push results vc
        let resultsVC = Constants.Storyboards.MainStoryboard.instantiateViewController(withIdentifier: "EventResultsVC") as! EventResultsVC
        resultsVC.event = event
        self.navigationController?.pushViewController(resultsVC, animated: true)
    }
    
    /// Cancel button
    func leftBarButtonTapped(sender:UIButton) {
        // show alert
        let alert = UIAlertController(title: "Cancel", message: "Do you really want to cancel this session? Event data will be lost.", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "No", style: .default, handler: nil))
        alert.addAction(UIAlertAction(title: "Yes", style: .destructive, handler: { [weak self] (alert) in
            // clear data
            self?.realm.beginWrite()
            self?.event.resetAllResults()
            try! self?.realm.commitWrite()
            // pop to root VC
            let _ = self?.navigationController?.popToRootViewController(animated: true)
        }))
        self.present(alert, animated: true, completion: nil)
    }
}

extension EventTimerVC: UICollectionViewDelegate, UICollectionViewDataSource {
    
    // MARK: UICollectionViewDataSource
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return event.athletes.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "AthleteCollectionViewCell", for: indexPath) as! AthleteCollectionViewCell
        let athlete = event.athletes[indexPath.row]
        cell.nameLabel.text = athlete.name
        
        // configure indicator
        for anIndicator in cell.indicators {
            // set colors
            if anIndicator.tag < athlete.fourthStateIndicatorValue() {
                anIndicator.backgroundColor = UIColor.appOnIndicatorColor()
            } else {
                anIndicator.backgroundColor = UIColor.appOffIndicatorColor()
            }
        }
        
        return cell
    }
    
    // MARK: UICollectionViewDelegate
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if redrawUItimer.isValid {
            self.playClickSound()
            
            let newResult = Result()
            if isRelayMode() {
                // Relay
                relayAthleteLabel.text = event?.athletes[indexPath.row].name
                
                if relaySelectedAthleteIndex == indexPath.row {
                    // Same athlete
                    newResult.lapTime = relayCurrentTime
                    newResult.overallTimeSplit = relayStartTime
                    print(newResult)
                } else {
                    // quick fix, need to refactor models
                    var PREV: TimeInterval = 0
                    
                    ///Trying to find interval "baton -> next"
                    if let prevAthlete = event.athletes[safe: relaySelectedAthleteIndex] {
                        // find inerval between two last items
                        let count = prevAthlete.results.count
                        if count > 0 {
                            PREV = (prevAthlete.results.last?.overallTimeSplit)! + (prevAthlete.results.last?.lapTime)!
                            let startTime = PREV//Date.timeIntervalSinceReferenceDate - interval
                            print(Date.timeIntervalSinceReferenceDate)
                            print(PREV)
                            relayStartTime = startTime
                        }
                    }
                    
                    // New athlete switched
                    relaySelectedAthleteIndex = indexPath.row
                    

                    
                    newResult.lapTime = relayCurrentTime
                    newResult.overallTimeSplit = relayStartTime
                }
            } else {
                // Classic lap/split
                newResult.lapTime = currentTime
                newResult.overallTimeSplit = startTime
            }
            
            realm.beginWrite()
            event.athletes[indexPath.row].results.append(newResult)
            try! realm.commitWrite()
            
            self.collectionView.reloadItems(at: [indexPath])
        }
    }
}

extension EventTimerVC: UICollectionViewDelegateFlowLayout {
    
    func itemsPerRow() -> CGFloat {
        return 2
    }
    
    func sectionInsets() -> UIEdgeInsets {
        return UIEdgeInsets(top: 8, left: 8, bottom: 8, right: 8)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let paddingSpace = sectionInsets().left * (itemsPerRow() + 1)
        let availableWidth = view.frame.width - paddingSpace
        let widthPerItem = availableWidth / itemsPerRow()
        
        return CGSize(width: widthPerItem, height: widthPerItem / 2)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return sectionInsets()
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return sectionInsets().left
    }
}
