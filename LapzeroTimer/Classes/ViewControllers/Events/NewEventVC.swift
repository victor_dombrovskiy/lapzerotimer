//
//  NewEventVC.swift
//  LapzeroTimer
//
//  Created by Victor on 9/20/16.
//  Copyright © 2016 Bananaapps. All rights reserved.
//

import Foundation
import UIKit
import RealmSwift

class NewEventVC: UIViewController {
    
    // MARK: - Properties
    
    @IBOutlet weak var relaySwitch: UISwitch! {
        didSet {
            relaySwitch.isOn = self.event.isRelay
        }
    }
    @IBOutlet weak var eventNameField: UITextField! {
        didSet {
            eventNameField.delegate = self
            eventNameField.autocapitalizationType = .sentences
        }
    }
    @IBOutlet weak var tableView: UITableView! {// tableview provides a list of runners
        didSet {
            tableView.dataSource = self
            tableView.delegate = self
            tableView.isEditing = true
        }
    }
    
    let realm = try! Realm()
    var event = Event()

    // MARK: - Initialization
    
    func configureOnload() {
        
        let doneButton = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(doneTapped(sender:)))
        navigationItem.rightBarButtonItems = [doneButton]
        
        // hide eyboard when tapping outside the text filed
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(tapSelfView(gesture:)))
        tapGesture.delegate = self
        self.view.addGestureRecognizer(tapGesture)
    }
    
    // MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureOnload()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        tableView.reloadData()
    }
    
    // MARK: - Actions
    
    func tapSelfView(gesture: UITapGestureRecognizer) {
        self.view.endEditing(true)
    }
    
    @IBAction func relayWSTogled(_ sender: UISwitch) {
        event.isRelay = !event.isRelay
    }
    
    func doneTapped(sender:UIButton) {
        // check name
        if eventNameField.text?.isEmpty ?? true {
            // empty
            self.showAlert(message: "Name field is empty")
        } else {
            event.name = eventNameField.text ?? ""
            
            // check runners
            if event.athletes.count < 1 {
                self.showAlert(message: "Add athletes")
            } else {
                // valid
                self.saveEventAndContinue()
            }
        }
    }
    
    @IBAction func addAthleteTapped(_ sender: UIButton) {
        let alert = UIAlertController(title: "New athlete", message: "", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        alert.addTextField { (tf) in
            tf.placeholder = "Enter athlete's name"
            tf.autocapitalizationType = .sentences
        }
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { [weak self] (action) in
            if let textField = alert.textFields?.first {
                if let text = textField.text {
                    if !text.isEmpty {
                        self?.addAthlete(name: textField.text!)
                    }
                }
            }
            }))
        self.present(alert, animated: true, completion: nil)
    }
    
    func addAthlete(name: String) {
        let newAthlete = Athlete()
        newAthlete.name = name
        event.athletes.append(newAthlete)
        tableView.reloadData()
    }
    
    func saveEventAndContinue() {
        // save
        realm.beginWrite()
        realm.add(event)
        try! realm.commitWrite()
        
        // push next vc
        let startEventVC = Constants.Storyboards.MainStoryboard.instantiateViewController(withIdentifier: "StartEventVC") as! StartEventVC
        startEventVC.event = event
        self.navigationController?.pushViewController(startEventVC, animated: true)
        
        // remove self from stack
        var viewControllers = self.navigationController!.viewControllers
        viewControllers.remove(at: viewControllers.count-2)
        self.navigationController?.viewControllers = viewControllers
    }
}

extension NewEventVC: UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return true
    }
}

extension NewEventVC: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return event.athletes.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "AthlettesListCell", for: indexPath) as! AthlettesListCell
        cell.nameLabel.text = event.athletes[indexPath.row].name
        cell.showsReorderControl = true
        return cell
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    // rearrange items
    
    func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, moveRowAt sourceIndexPath: IndexPath, to destinationIndexPath: IndexPath) {
        let item = event.athletes[sourceIndexPath.row]
        event.athletes.remove(at: sourceIndexPath.row)
        event.athletes.insert(item, at: destinationIndexPath.row)
    }
    
}

extension NewEventVC: UITableViewDelegate {
    
    @objc(tableView:commitEditingStyle:forRowAtIndexPath:) func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            event.athletes.remove(at: indexPath.row)
            tableView.deleteRows(at: [indexPath], with: .fade)
        }
    }
    
}

extension NewEventVC: UIGestureRecognizerDelegate {
    
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool {
        self.view.endEditing(true)
        return false
    }
}
