//
//  EventsVC.swift
//  LapzeroTimer
//
//  Created by Victor on 9/20/16.
//  Copyright © 2016 Bananaapps. All rights reserved.
//

import Foundation
import UIKit
import RealmSwift

class EventsVC: UIViewController {
    
    // MARK: - Properties
    
    @IBOutlet weak var tableView: UITableView!
    
    let realm = try! Realm()
    let results = try! Realm().objects(Event.self).sorted(byProperty: "date")
    var notificationToken: NotificationToken? // called on changes
    
    // MARK: - Initialization
    
    func configureOnload() {
        
        let rightBarButton = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(rightBarButtonTapped(sender:)))
        navigationItem.rightBarButtonItems = [rightBarButton]
        
        tableView.dataSource = self
        tableView.delegate = self
        
        // Set results notification block
        self.notificationToken = results.addNotificationBlock { (changes: RealmCollectionChange) in
            switch changes {
            case .initial:
                self.tableView.reloadData()
                break
            case .update(_, _, _, _):
                self.tableView.reloadData()
                break
            case .error(let err):
                fatalError("\(err)")
                break
            }
        }
    }
    
    // MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureOnload()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        tableView.reloadData()
    }
    
    // MARK: - Actions
    
    func rightBarButtonTapped(sender:UIButton) {
        let newEventVC = Constants.Storyboards.MainStoryboard.instantiateViewController(withIdentifier: "NewEventVC") as! NewEventVC
        self.navigationController?.pushViewController(newEventVC, animated: true)
    }
    
    func selectEvent(event: Event) {
        // TODO: check this
        
//        // clear previous session
//        event.resetAllResults()
//        DataManager.sharedInstance.saveCommitedCahngesData()
        
        let startEventVC = Constants.Storyboards.MainStoryboard.instantiateViewController(withIdentifier: "StartEventVC") as! StartEventVC
        startEventVC.event = event
        self.navigationController?.pushViewController(startEventVC, animated: true)
    }
    
}


extension EventsVC: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return results.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ResultsCell", for: indexPath) as! ResultsCell
        
        let event = results[indexPath.row]
        cell.nameLabel.text = event.name
        cell.dateLabel.text = event.date.toStringAppFormatted()
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
}

extension EventsVC: UITableViewDelegate {
    
    @objc(tableView:commitEditingStyle:forRowAtIndexPath:) func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            realm.beginWrite()
            realm.delete(results[indexPath.row])
            try! realm.commitWrite()
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let event = results[indexPath.row]
        self.selectEvent(event: event)
    }
    
}
