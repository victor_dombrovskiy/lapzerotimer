//
//  ResultsVC.swift
//  LapzeroTimer
//
//  Created by Victor on 9/20/16.
//  Copyright © 2016 Bananaapps. All rights reserved.
//

import Foundation
import UIKit
import RealmSwift

class ResultsVC: UIViewController {
    
    // MARK: - Properties
    
    @IBOutlet weak var tableView: UITableView!
    
    let realm = try! Realm()
    let events = try! Realm().objects(Event.self).sorted(byProperty: "date")
    let classicEvents = try! Realm().objects(ClassicTimerEvent.self).sorted(byProperty: "date")
    var notificationTokenEvents: NotificationToken? // called on changes
    var notificationTokenClassicEvents: NotificationToken? // called on changes
    
    // MARK: - Initialization
    
    func configureOnload() {
        
        tableView.dataSource = self
        tableView.delegate = self
        
        // Set results notification blocks
        self.notificationTokenEvents = events.addNotificationBlock { (changes: RealmCollectionChange) in
            switch changes {
            case .initial:
                self.tableView.reloadData()
                break
            case .update(_, _, _, _):
                self.tableView.reloadData()
                break
            case .error(let err):
                fatalError("\(err)")
                break
            }
        }
        self.notificationTokenClassicEvents = classicEvents.addNotificationBlock { (changes: RealmCollectionChange) in
            switch changes {
            case .initial:
                self.tableView.reloadData()
                break
            case .update(_, _, _, _):
                self.tableView.reloadData()
                break
            case .error(let err):
                fatalError("\(err)")
                break
            }
        }
    }
    
    // MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureOnload()
    }
    
    // MARK: - Actions
    
    func selectEvent(event: EventViewModelProtocol) {
        let resultsVC = Constants.Storyboards.MainStoryboard.instantiateViewController(withIdentifier: "ResultDetailsVC") as! ResultDetailsVC
        resultsVC.event = event
        self.navigationController?.pushViewController(resultsVC, animated: true)
    }
}

extension ResultsVC: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return events.count
        } else {
            return classicEvents.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ResultsCell", for: indexPath) as! ResultsCell
        
        var event: EventViewModelProtocol
        if indexPath.section == 0 {
            event = events[indexPath.row]
        } else {
            event = classicEvents[indexPath.row]
        }
        
        cell.nameLabel.text = event.nameOfEvent()
        cell.dateLabel.text = event.dateOfEvent().toStringAppFormatted()
        return cell
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if section == 0 {
            return "Events"
        } else {
            return "Classic timer results"
        }
    }
    
}

extension ResultsVC: UITableViewDelegate {
    
    @objc(tableView:commitEditingStyle:forRowAtIndexPath:) func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        // Delete the row from the data source
        if editingStyle == .delete {
            realm.beginWrite()
            if indexPath.section == 0 {
                // "Events"
                realm.delete(events[indexPath.row])
            } else {
                // "Classic timer results"
                realm.delete(classicEvents[indexPath.row])
            }
            // Delete the row from the data source
            try! realm.commitWrite()
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == 0 {
            // "Events"
            self.selectEvent(event: events[indexPath.row])
        } else {
            // "Classic timer results"
            self.selectEvent(event: classicEvents[indexPath.row])
        }
    }
    
}
