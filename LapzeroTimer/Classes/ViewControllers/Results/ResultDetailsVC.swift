//
//  ResultDetailsVC.swift
//  LapzeroTimer
//
//  Created by Victor on 9/20/16.
//  Copyright © 2016 Bananaapps. All rights reserved.
//

import Foundation
import UIKit

class ResultDetailsVC: UIViewController {
    
    // MARK: - Properties
    
    var event: EventViewModelProtocol!
    @IBOutlet weak var textView: UITextView!
    
    // MARK: - Initialization
    
    func configureOnload() {
        
        let rightBarButton = UIBarButtonItem(image: #imageLiteral(resourceName: "Share-bar"), style: .plain, target: self, action: #selector(rightBarButtonTapped(sender:)))
        navigationItem.rightBarButtonItems = [rightBarButton]
        
        self.navigationItem.title = event.nameOfEvent()
    }
    
    // MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureOnload()
        textView.attributedText = event.resultsStringFormatted()
    }
    
    // UITextView text content doesn't start from the top, solution:
    override func viewWillAppear(_ animated: Bool) {
        textView.isScrollEnabled = false
    }
    // and this one too:
    override func viewDidAppear(_ animated: Bool) {
        textView.isScrollEnabled = true
    }
    
    // MARK: - Actions
    
    func rightBarButtonTapped(sender:UIButton) {
        self.presentActivityShareMenu(activityItems: [event.resultsStringFormatted()], sourceView: sender)
    }
}
