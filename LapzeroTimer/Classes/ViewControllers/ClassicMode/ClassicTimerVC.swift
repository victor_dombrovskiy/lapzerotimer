//
//  ClassicTimerVC.swift
//  LapzeroTimer
//
//  Created by Victor on 9/20/16.
//  Copyright © 2016 Bananaapps. All rights reserved.
//

import Foundation
import UIKit
import RealmSwift
import KRProgressHUD

class ClassicTimerVC: UIViewController {
    
    // Constants
    private struct Txt {
        static let start = "START"
        static let stop = "STOP"
        static let lapSplit = "LAP/SPLIT"
    }
    
    // MARK: - Properties
    
    // Outlets
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var lapLabel: UILabel! {
        didSet {
            lapLabel.font = lapLabel.font.monospacedDigitFont
        }
    }
    @IBOutlet weak var splitLabel: UILabel! {
        didSet {
            splitLabel.font = splitLabel.font.monospacedDigitFont
        }
    }
    @IBOutlet weak var lapSplitButton: UIButton! {
        didSet {
            lapSplitButton.backgroundColor = UIColor.appBlueColor()
            lapSplitButton.layer.cornerRadius = 4
            lapSplitButton.clipsToBounds = true
            lapSplitButton.setTitle(Txt.lapSplit, for: .normal)
            // initial state
            lapSplitButton.isEnabled = false
        }
    }
    @IBOutlet weak var startStopButton: UIButton! {
        didSet {
            startStopButton.backgroundColor = UIColor.appGreenColor()
            startStopButton.layer.cornerRadius = 4
            startStopButton.clipsToBounds = true
            // initial state
            startStopButton.setTitle(Txt.start, for: .normal)
            startStopButton.backgroundColor = UIColor.appGreenColor()
        }
    }
    
    var rightBarbuttonsUnlocked: Bool = false {
        didSet {
            navigationItem.rightBarButtonItems?.first?.isEnabled = rightBarbuttonsUnlocked
            navigationItem.rightBarButtonItems?.last?.isEnabled = rightBarbuttonsUnlocked
        }
    }
    
    // Timers
    var mainTimer = Timer() // main timer
    var splitCurrentTime: TimeInterval = 0 // Split. total time. += timer.timeInterval
    var mainTimerIsPaused: Bool = false
    
    // Lap:
    var lapStartTime: TimeInterval = 0
    var lapCurrentTime: TimeInterval {
        get {
            return self.splitCurrentTime - self.lapStartTime
        }
    }
    
    // Data
    var event = ClassicTimerEvent()
    let realm = try! Realm()
    
    // MARK: - Initialization
    
    func configureOnload() {
        self.navigationItem.title = "Classic mode"
        
        let shareButton = UIBarButtonItem(image: #imageLiteral(resourceName: "Share-bar"), style: .plain, target: self, action: #selector(shareButtonTapped(sender:)))
        let saveButton = UIBarButtonItem(image: #imageLiteral(resourceName: "Save-bar"), style: .plain, target: self, action: #selector(saveButtonTapped(sender:)))
        navigationItem.rightBarButtonItems = [saveButton, shareButton]
        
        let leftBarButton = UIBarButtonItem(title: "Reset", style: .plain, target: self, action: #selector(resetButtonTapped(sender:)))
        navigationItem.leftBarButtonItem = leftBarButton
        
        splitLabel.text = 0.stringFormatted()
        lapLabel.text = 0.stringFormatted()
        
        tableView.delegate = self
        tableView.dataSource = self
    }
    
    // MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureOnload()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        // disable sleep once VC started
        Constants.AppDelegate.preventSleepMode(prevent: true)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        // enable sleep mode
        Constants.AppDelegate.preventSleepMode(prevent: false)
        super.viewWillDisappear(animated)
    }
    
    // MARK: - Timers
    
    func startResumeTimer() {
        // start
        mainTimer = Timer.scheduledTimer(timeInterval: 0.01, target: self, selector: #selector(timerFired(_:)), userInfo: nil, repeats: true)
        RunLoop.current.add(mainTimer, forMode: .commonModes)
        self.redrawTimerAction()
    }
    
    func stopTimer() {
        // hm, means "pause"
        mainTimerIsPaused = true
        mainTimer.invalidate()
    }
    
    func resetTimer() {
        // update button
        if mainTimer.isValid {
            // button state
            startStopButton.setTitle(Txt.start, for: .normal)
            startStopButton.backgroundColor = UIColor.appGreenColor()
            lapSplitButton.isEnabled = false
            // unlock tabbar
            Constants.AppDelegate.tabBarController.isLocked = false
            // Unlock "share" and "save" buttons
            rightBarbuttonsUnlocked = true
        }
        
        // clear data
        mainTimer.invalidate()
        event = ClassicTimerEvent()
        splitCurrentTime = 0
        lapStartTime = 0
        tableView.reloadData()
        redrawTimerAction()
    }
    
    func redrawTimerAction() {
        splitLabel.text = splitCurrentTime.stringFormatted()
        lapLabel.text = lapCurrentTime.stringFormatted()
    }
    
    func timerFired(_ timer: Timer) {
        splitCurrentTime += timer.timeInterval
        redrawTimerAction()
    }
    
    // MARK: - Actions
    
    // MARK: Bar buttons
    
    func shareButtonTapped(sender:UIButton) {
        if event.results.count == 0 {
            self.showAlert(message: "No results, can't share")
        } else {
            self.presentActivityShareMenu(activityItems: [event.resultsStringFormatted()], sourceView: sender)
        }
    }
    
    func saveButtonTapped(sender:UIButton) {
        if event.results.count == 0 {
            self.showAlert(message: "Nothing to save")
        } else {
            // Save results
            realm.beginWrite()
            realm.add(event)
            do {
                try realm.commitWrite()
                KRProgressHUD.showSuccess()
                self.resetTimer()
            } catch {
                // handle error
                KRProgressHUD.showError()
            }
        }
    }
    
    func resetButtonTapped(sender:UIButton) {
        self.resetTimer()
    }
    
    // MARK: start-stop, lap-split buttons
    
    @IBAction func startStopButtonTap(_ sender: UIButton) {
        self.playClickSound()
        // check if running
        if mainTimer.isValid {
            // should STOP
            self.stopTimer()
            // button state
            startStopButton.setTitle(Txt.start, for: .normal)
            startStopButton.backgroundColor = UIColor.appGreenColor()
            lapSplitButton.isEnabled = false
            
            // unlock tabbar
            Constants.AppDelegate.tabBarController.isLocked = false
            
            // Unlock "share" and "save" buttons
            rightBarbuttonsUnlocked = true
        } else {
            // should START
            self.startResumeTimer()
            // button state
            startStopButton.setTitle(Txt.stop, for: .normal)
            startStopButton.backgroundColor = UIColor.appRedColor()
            lapSplitButton.isEnabled = true
            
            // lock tabbar
            Constants.AppDelegate.tabBarController.isLocked = true
            
            // lock "share" and "save" buttons
            rightBarbuttonsUnlocked = false
        }
    }
    
    @IBAction func lapSplitButtonTap(_ sender: UIButton) {
        self.playClickSound()
        // check if active timer
        if mainTimer.isValid {
            realm.beginWrite()
            let aResult = Result()
            aResult.lapTime = lapCurrentTime
            aResult.overallTimeSplit = splitCurrentTime
            event.results.append(aResult)
            try! realm.commitWrite()
            lapStartTime = splitCurrentTime
            tableView.reloadData()
            
            // scroll to bottom
            DispatchQueue.main.async { [weak self] in
                let count = self?.event.results.count ?? 0
                let indexPath = IndexPath(row: count - 1, section: 0)
                self?.tableView.scrollToRow(at: indexPath, at: .bottom, animated: true)
            }
        }
    }
}

extension ClassicTimerVC: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return event.results.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "default", for: indexPath)
        let aResult = event.results[indexPath.row]
        let count = String(format: "%02d\t", indexPath.row + 1)
        cell.textLabel?.text = count + "\t" + aResult.lapTime.stringFormatted() + "\t" + aResult.overallTimeSplit.stringFormatted()
        cell.textLabel?.font = UIFont.boldSystemFont(ofSize: 18)
        cell.selectionStyle = .none
        return cell
    }
    
}


