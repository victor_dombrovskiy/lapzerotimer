//
//  Result.swift
//  LapzeroTimer
//
//  Created by Victor on 10/1/16.
//  Copyright © 2016 Bananaapps. All rights reserved.
//

import Foundation
import RealmSwift

// MARK: - Result model

class Result: Object {
    dynamic var lapTime: TimeInterval = 0
    dynamic var overallTimeSplit: TimeInterval = 0
}
