//
//  ClassicTimerEvent.swift
//  LapzeroTimer
//
//  Created by Victor on 10/1/16.
//  Copyright © 2016 Bananaapps. All rights reserved.
//

import Foundation
import RealmSwift

// MARK: - ClassicTimerEvent model

class ClassicTimerEvent: Object {
    
    // default values
    dynamic var name: String = "Event at " + Date().toHoursMinutesString()
    dynamic var date: Date = Date()
    
    var results = List<Result>()
}

// MARK: - View model

extension ClassicTimerEvent: EventViewModelProtocol {
    
    func dateOfEvent() -> Date {
        return self.date
    }
    
    func nameOfEvent() -> String {
        return self.name
    }
    
    func resultsStringFormatted() -> NSAttributedString {
        let attributedString = NSMutableAttributedString()
        let fontSize: CGFloat = 18
        let headerTextAttr = [ NSForegroundColorAttributeName: UIColor.black,
                               NSFontAttributeName: UIFont.boldSystemFont(ofSize: fontSize)]
        let blackTextAttr = [ NSForegroundColorAttributeName: UIColor.black,
                              NSFontAttributeName: UIFont.systemFont(ofSize: fontSize)]
        
        // add name
        attributedString.append(NSAttributedString(string: (self.name + ":\n\n"), attributes: headerTextAttr))
        var counter: Int = 0
        for aResult in self.results {
            // add results:
            attributedString.append(NSAttributedString(string: String(format: "%02d\t", counter), attributes: blackTextAttr))
            // lap
            attributedString.append(NSAttributedString(string: (aResult.lapTime.stringFormatted() + "\t"), attributes: blackTextAttr))
            // total time
            attributedString.append(NSAttributedString(string: (aResult.overallTimeSplit.stringFormatted() + "\n"), attributes: blackTextAttr))
            //
            counter = counter + 1
        }
        
        return attributedString
    }
}
