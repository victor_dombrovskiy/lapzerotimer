//
//  Athlete.swift
//  LapzeroTimer
//
//  Created by Victor on 10/1/16.
//  Copyright © 2016 Bananaapps. All rights reserved.
//

import Foundation
import RealmSwift

// MARK: - Athlete model

class Athlete: Object {
    dynamic var name: String = "Athlete"
    var results = List<Result>()
}

// MARK: - View model

extension Athlete {
    // may be [0:4], 0 means-off, 4 means - all on
    func fourthStateIndicatorValue() -> Int {
        if self.results.count > 0 {
            // convert to [1:4]
            let count = self.results.count
            let divided = Float(count) / 4
            let remainder = divided.truncatingRemainder(dividingBy: 1)
            if remainder == 0 {
                return 4
            } else {
                let value: Int = Int(remainder * 4)
                return value
            }
        } else {
            return 0
        }
    }
}
