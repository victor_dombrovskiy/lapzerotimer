//
//  Event.swift
//  LapzeroTimer
//
//  Created by Victor on 9/21/16.
//  Copyright © 2016 Bananaapps. All rights reserved.
//

import Foundation
import RealmSwift

protocol EventViewModelProtocol {
    func dateOfEvent() -> Date
    func nameOfEvent() -> String
    func resultsStringFormatted() -> NSAttributedString
}

// MARK: - Event model


class Event: Object {
    dynamic var name: String = "New event"
    dynamic var date: Date = Date()
    dynamic var isRelay: Bool = false
    var athletes = List<Athlete>()
}

// data management 
extension Event {
    func resetAllResults() {
        for i in 0...self.athletes.count - 1 {
            self.athletes[i].results.removeAll()
        }
    }
}

// MARK: - View model

extension Event: EventViewModelProtocol {
    
    func dateOfEvent() -> Date {
        return self.date
    }
    
    func nameOfEvent() -> String {
        return self.name
    }
    
    func resultsStringFormatted() -> NSAttributedString {
        let attributedString = NSMutableAttributedString()
        let fontSize: CGFloat = 18
        let titleStyle = NSMutableParagraphStyle()
        titleStyle.alignment = .center
        let titleTextAttr = [ NSForegroundColorAttributeName: UIColor.black,
                               NSFontAttributeName: UIFont.boldSystemFont(ofSize: fontSize + 2),
                               NSParagraphStyleAttributeName: titleStyle]
        let headerTextAttr = [ NSForegroundColorAttributeName: UIColor.black,
                               NSFontAttributeName: UIFont.boldSystemFont(ofSize: fontSize)]
        let blackTextAttr = [ NSForegroundColorAttributeName: UIColor.black,
                              NSFontAttributeName: UIFont.systemFont(ofSize: fontSize)]
        let redTextAttr = [ NSForegroundColorAttributeName: UIColor.appRedColor(),
                            NSFontAttributeName: UIFont.systemFont(ofSize: fontSize) ]
        // name of event
        attributedString.append(NSAttributedString(string: (self.name + ":\n\n"), attributes: titleTextAttr))
        for athlete in self.athletes {
            // add athlete name:
            attributedString.append(NSAttributedString(string: (athlete.name + "\n"), attributes: headerTextAttr))
            
            // build output strings:
            var firesString = ""
            var intervalsString = ""
            
            var prevResultIteration: Double = 0
            for aResult in athlete.results {
                firesString = firesString + aResult.lapTime.stringFormatted() + " "
                let interval = aResult.lapTime - prevResultIteration
                intervalsString = intervalsString + interval.stringFormatted() + " "
                prevResultIteration = aResult.lapTime
            }
            
            // add results strings:
            attributedString.append(NSAttributedString(string: (intervalsString + "\n"), attributes: blackTextAttr))
            attributedString.append(NSAttributedString(string: (firesString + "\n\n"), attributes: redTextAttr))
        }

        // if relay:
        if self.isRelay {
            var totalTime: TimeInterval = 0
            for athlete in self.athletes {
                if let result = athlete.results.last {
                    totalTime = totalTime + result.lapTime
                }
            }
            attributedString.append(NSAttributedString(string: ("\n\nTotal: " + totalTime.stringFormatted()), attributes: blackTextAttr))
        }
        
        return attributedString
    }
}
