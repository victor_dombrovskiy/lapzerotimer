//
//  Constants.swift
//  LapzeroTimer
//
//  Created by Victor on 9/19/16.
//  Copyright © 2016 Bananaapps. All rights reserved.
//

import Foundation
import UIKit

/// Global constants file
struct Constants {
    
    /*******************************
     *                              *
     *       Global constants       *
     *                              *
     *******************************/
    
    static let AppDelegate = UIApplication.shared.delegate as! AppDelegate
    
    struct NotificationNames {
        static let dataManagerUpdates = Notification.Name("postNotificationDataUpdates")
    }
    
    struct Storyboards {
        static let MainStoryboard:UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
    }
    
}

/// Color constants
extension UIColor {
    
    // Picked from logo:
    
    class func appMainColor() -> UIColor {
        return UIColor(hex: 0x514398)//UIColor(red:0.23, green:0.31, blue:0.39, alpha:1.00)
    }
    
    class func appGrayColor() -> UIColor {
        return UIColor(red:0.63, green:0.63, blue:0.63, alpha:1.00)
    }
    
    class func appLightGrayColor() -> UIColor {
        return UIColor(red:0.93, green:0.93, blue:0.95, alpha:1.00)
    }
    
    class func appWhiteLikeColor() -> UIColor {
        return UIColor(red:1.0, green:1.0, blue:1.0, alpha:1.00)
    }
    
    class func appPurpleColor() -> UIColor {
        return UIColor(red:0.33, green:0.25, blue:0.59, alpha:1.00)
    }
    
    // end
    
    class func appGreenColor() -> UIColor {
        return UIColor(red:0.00, green:0.77, blue:0.21, alpha:1.00)
    }
    
    class func appRedColor() -> UIColor {
        return UIColor(red:0.91, green:0.19, blue:0.24, alpha:1.00)
    }
    
    class func appBlueColor() -> UIColor {
        return UIColor(red:0.00, green:0.42, blue:1.00, alpha:1.00)
    }
    
    class func appYellowColor() -> UIColor {
        return UIColor(hex: 0xFAC028)
    }
    
    // timer indicator:
    
    class func appOnIndicatorColor() -> UIColor {
        return UIColor.appGrayColor().withAlphaComponent(0.9)//UIColor.appGreenColor()
    }
    
    class func appOffIndicatorColor() -> UIColor {
        return UIColor.appGrayColor().withAlphaComponent(0)//UIColor.appGrayColor()
    }
    
    // Indicator box
    
    class func appIndicatorBoxColor() -> UIColor {
        return UIColor.appYellowColor()
    }

}
