//
//  UITextView+Helpers.swift
//  LapzeroTimer
//
//  Created by Victor on 9/30/16.
//  Copyright © 2016 Bananaapps. All rights reserved.
//

import Foundation
import UIKit

extension UITextView {
    func scrollToBottom() {
        let range = NSMakeRange(self.text.characters.count - 1, 0)
        self.scrollRangeToVisible(range)
    }
}
