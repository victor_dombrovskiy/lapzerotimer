//
//  Timer+Extensions.swift
//  LapzeroTimer
//
//  Created by Victor on 9/23/16.
//  Copyright © 2016 Bananaapps. All rights reserved.
//

import Foundation

extension TimeInterval {
    
    /// Rounds the double to decimal places value
    func roundTo(places:Int) -> Double {
        let divisor = pow(10.0, Double(places))
        return (self * divisor).rounded() / divisor
    }
}
