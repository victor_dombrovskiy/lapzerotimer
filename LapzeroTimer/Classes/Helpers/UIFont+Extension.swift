//
//  UIFont+Extension.swift
//  LapzeroTimer
//
//  Created by Victor on 9/20/16.
//  Copyright © 2016 Bananaapps. All rights reserved.
//

import Foundation
import UIKit

///Monospaced numbers for UILabel
extension UIFont {
    
    var monospacedDigitFont: UIFont {
        let fontDescriptorFeatureSettings = [[UIFontFeatureTypeIdentifierKey: kNumberSpacingType, UIFontFeatureSelectorIdentifierKey: kMonospacedNumbersSelector]]
        let fontDescriptorAttributes = [UIFontDescriptorFeatureSettingsAttribute: fontDescriptorFeatureSettings]
        let oldFontDescriptor = fontDescriptor
        let newFontDescriptor = oldFontDescriptor.addingAttributes(fontDescriptorAttributes)
        
        return UIFont(descriptor: newFontDescriptor, size: 0)
    }
}
