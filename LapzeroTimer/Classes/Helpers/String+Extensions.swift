//
//  String+Extensions.swift
//  LapzeroTimer
//
//  Created by Victor on 9/21/16.
//  Copyright © 2016 Bananaapps. All rights reserved.
//

import Foundation

extension String {
    
    ///For placeholders
    static func randomAlphaNumericString(length: Int) -> String {
        let charactersString = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
        let charactersArray : [Character] = Array(charactersString.characters)
        
        var string = ""
        for _ in 0..<length {
            string.append(charactersArray[Int(arc4random()) % charactersArray.count])
        }
        
        return string
    }
}
