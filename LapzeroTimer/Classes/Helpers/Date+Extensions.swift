//
//  Date+Extensions.swift
//  LapzeroTimer
//
//  Created by Victor on 9/30/16.
//  Copyright © 2016 Bananaapps. All rights reserved.
//

import Foundation

extension Date {
    
    /// For application labels 
    func toStringAppFormatted() -> String {
        let components = NSCalendar.current.dateComponents([Calendar.Component.day,
                                                            Calendar.Component.month,
                                                            Calendar.Component.year],
                                                           from: self)
        return "\(components.day!).\(components.month!).\(components.year!)"
    }
    
    /// get local time
    func toHoursMinutesString() -> String {
        let components = NSCalendar.current.dateComponents([Calendar.Component.hour,
                                                            Calendar.Component.minute],
                                                           from: self)
        return String(format: "%02d:%02d", components.hour!, components.minute!)
    }
}
