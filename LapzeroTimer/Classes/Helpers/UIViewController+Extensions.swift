//
//  UIViewController+Extensions.swift
//  LapzeroTimer
//
//  Created by Victor on 9/22/16.
//  Copyright © 2016 Bananaapps. All rights reserved.
//

import Foundation
import UIKit
import AVFoundation

extension UIViewController {
    
    func showAlert(message: String) {
        let alert = UIAlertController(title: "Message", message:message, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler:{ (ACTION :UIAlertAction!)in
            print("User click Ok button")
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    func playClickSound() {
        if let soundUrl = Bundle.main.url(forResource: "iPodClick", withExtension: "aiff") {
            var sound: SystemSoundID = 0
            AudioServicesCreateSystemSoundID(soundUrl as CFURL, &sound)
            AudioServicesPlaySystemSound(sound);
        }
        // haptic feedback
        if #available(iOS 10.0, *) {
            let generator = UINotificationFeedbackGenerator()
            generator.notificationOccurred(.success)
        } else {
            // Fallback on earlier versions
        }
    }
}

extension UIViewController {
    
    func presentActivityShareMenu(activityItems: [Any], sourceView: UIView) {
        let shareVC = UIActivityViewController(activityItems: activityItems, applicationActivities: nil)
        shareVC.popoverPresentationController?.sourceView = sourceView
        self.present(shareVC, animated: true, completion: nil)
    }
}
