//
//  AppDelegate.swift
//  LapzeroTimer
//
//  Created by Victor on 9/19/16.
//  Copyright © 2016 Bananaapps. All rights reserved.
//

import UIKit
import AVFoundation

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    var tabBarController: TabBarVC = Constants.Storyboards.MainStoryboard.instantiateViewController(withIdentifier: "TabBarVC") as! TabBarVC


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        
        // Initialization:
        self.setupViewControllers()
        self.configureAppearence()
        
        // preload sounds
        if let soundUrl = Bundle.main.url(forResource: "Silence", withExtension: "aiff") {
            var sound: SystemSoundID = 1
            AudioServicesCreateSystemSoundID(soundUrl as CFURL, &sound)
            AudioServicesPlaySystemSound(sound);
        }
        
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    
}

// MARK: - Do initialization stuff

extension AppDelegate {
    
    fileprivate func setupViewControllers() {
        self.window = UIWindow(frame: UIScreen.main.bounds)
        self.window?.rootViewController = tabBarController
        self.window?.makeKeyAndVisible()
    }
    
    fileprivate func configureAppearence() {
        // If you targeting iOS 9 - dont change this:
        UIApplication.shared.setStatusBarStyle(.lightContent, animated: false)
        
        // Appearence proxies:
        // UINavigation Bar
        UINavigationBar.appearance().isTranslucent = false
        UINavigationBar.appearance().shadowImage = UIImage()
        UINavigationBar.appearance().barTintColor = UIColor.appMainColor()
        UINavigationBar.appearance().tintColor = UIColor.white
        UINavigationBar.appearance().titleTextAttributes = [NSForegroundColorAttributeName : UIColor.white]
        
        // Set Navigation bar background image
        UINavigationBar.appearance().setBackgroundImage(#imageLiteral(resourceName: "NavBarBackground"), for: .default)
        
        // UITabBar customization
        UITabBar.appearance().isTranslucent = true
        UITabBar.appearance().barTintColor = UIColor.appLightGrayColor()
        UITabBar.appearance().tintColor = UIColor.appMainColor()
    }
    
}

// MARK: - Disable sleep mode for app

extension AppDelegate {
    
    /// disables sleep mode for the app
    ///
    /// - parameter allowed: Booleat flag
    func preventSleepMode(prevent: Bool) {
        // a trick, you must set it NO before, it's apple's bug in some iOS versions
        UIApplication.shared.isIdleTimerDisabled = false
        UIApplication.shared.isIdleTimerDisabled = prevent
    }

}
